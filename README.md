GBT-SC IP - Example designs
===================
The **GBT-FPGA** is a VHDL-based IP designed to offer a back-end counterpart to the GBTx ASIC. It is a common project, used to communicate with both GBTx (serdes) and GBT-SCA (Slow Control Adapter) Radiation tolerant ASICs as defined by the GBT architecture. In the framework of this project, a new module named **GBT-SC** has been designed and released to handle the **slow control** fields hosted in the GBT frame for Internal Control (GBTx) and External Control (SCA). 

> **Note:** This repository contains example designs not supported by the GBT-FPGA team. They are updated according to the GBT-SC releases but no additional FPGAs will be added. The GBT-SC core is device agnostic and can be easily instantiate in your design.

# Outline:

* [Repository architecture](#repository-architecture)
* [GBT-SC architecture](#gbt-sc-architecture)
    * [SCA control UML](#sca-control-sequence)
    * [GBTx internal register - write access](#gbtx-internal-register-write-access)
    * [GBTx internal register - read access](#gbtx-internal-register-read-access)
* [Example designs](#example-designs)
    * [Xilinx (VC707)](#xilinx-vc707)
    * [Altera (Startix V)](#altera-stratix-v)



# Repository architecture

The main directory of the GIT repository contains the following folders:

 - GBT Bank: This directory contains the core of the GBT-FPGA v.5.0.0 IP.
 - GBT SC: This directory is a submodule linked to the gbt-sc git repository. Before using the example designs, the submodule update command must be used to pull the its content.
 - Example designs: This directory contains the examples designs described in the [example design](#example-designs) section.

> **Tip:** Before using the example designs, the users have to update the GBT-SC submodule using the following terminal commands:
> ```
> cd gbtsc-example-designs
> git submodule init
> git submodule update
> ```
> Or  using tortoise git:
> ```
> right click > TortoiseGit > Submodule Update
> ```

# GBT-SC architecture

The GBT-SC has been designed as a stand-alone module that can be used either with the GBT-FPGA or an e-link serializer. It was thought in a way to provide a common core for all of the slow-control defined by the GBT ASICs: GBTx configuration via the Internal Control field (IC) and SCA configuration via the External Control field (EC). To be as much generic as possible, the number of SCA fields as well as the number of IC fields that the module can manage is defined by a generic parameter. Then, an input can be used to select which feilds must be active when a command is being sent.

## SCA control sequence:

<p align="center">
    <img src="https://gitlab.cern.ch/gbtsc-fpga-support/gbtsc-example-designs/-/raw/master/img/SCA_ulm.png">
</p>

> **Note:**

> In order to optimize the configuration time, a same command can be sent to multiple GBT-SCA thanks to the enable bus. Each bit of the bus control one "EC field" output. The number of the "EC lines" depends on the value of the g_SCA_COUNT parameter.

## GBTx internal register - write access:

<p align="center">
    <img src="https://gitlab.cern.ch/gbtsc-fpga-support/gbtsc-example-designs/-/raw/master/img/IC_Write_ulm.png">
</p>

## GBTx internal register - read access:

<p align="center">
    <img src="https://gitlab.cern.ch/gbtsc-fpga-support/gbtsc-example-designs/-/raw/master/img/IC_Read_ulm.png">
</p>

# Example designs

Two example designs are available in this repository: one for Virtex 7 (Xilinx) and another one for Stratix V (Altera).

## Xilinx (VC707)

The VC707 example design implements the GBT-FPGA v.5.0.0 and the latest version of the GBT-SC IP (submodule of the git repository).

### Firmware

The GBT-SC module is controlled using a JTAG to AXI module connected to the GBT-SC controller instance. It contains the following registers that are connected to the GBT-SC IP:

**Write registers:**

> **x"00000000":**
> > [ 31:24 ] : Command <br/>
> > [ 23:16 ] : Length <br/>
> > [ 15:8 ]  : Channel <br/>
> > [ 7:0 ]   : Transaction ID 
> 
> **x"00000004":**
> > Data (sends start command)
> 
> **x"00000008":**
> > x"00000000"   : Reset GBT-SC IP <br/>
> > x"00000001"   : Send connect command <br/>
> > x"00000002"   : Send reset command 
> 
> **x"0000000c":**
> > [ 15:8 ]  : x"01" (Read) or x"02" (Write) <br/>
> > [ 7:0 ]   : GBTx address 
> 
> **x"00000010":**
> > [ 31:16 ] : Nb data to be read <br/>
> > [ 15:0 ]  : GBTx Internal register 
> 
> **x"00000014":**
> > [ 31:0 ]  : Register value (automatically written into the Tx FIFO)
> 

**Read registers:**

> **x"00000000":**
> > [ 31:24 ] : Transaction ID (reply) <br/>
> > [ 23:16 ] : Channel (reply) <br/>
> > [ 15:8 ]  : Length  (reply) <br/>
> > [ 7:0 ]   : Error (reply) <br/>
>  
> **x"00000004":**
> > Data (reply)
> 
> **x"00000008":**
> > [ 0 ]   : Rx reply received flag
> 
> **x"0000000c":**
> > [ 7:0 ]   : GBTx address (reply)
> 
> **x"00000010":**
> > [ 31:16 ] : Nb of words (reply) <br/>
> > [ 15:0 ]  : First GBTx Internal register address (reply) <br/>
> 
> **x"00000014":**
> > [ 1 ]  : Ready (IC) <br/>
> > [ 0 ] : Rx fifo empty
> 
> **x"00000018":**
> > Data (reply). Automatically send a read command to the Rx FIFO
> 

### Software

The TCL scripts used to control the GBT-SC IP are located into the example_designs/xilinx_k7v7/tcl directory. It includes one low level library that describes the procedure to read an write a register of the gbt-sc controller instance as well as a low level libray for the GBT-SCA and another for the GBTx IC. Additional example scripts to control the GPIO and the I2C interfaces are available.

## Altera (Stratix V)

The Stratix V example design implements the GBT-FPGA v.5.0.0 and the latest version of the GBT-SC IP (submodule of the git repository).

### Firmware

The GBT-SC module is controlled using a PCIe to Avalon Memory-Mapper IP connected to the GBT-SC controller instance. It contains the following registers that are connected to the GBT-SC IP:

**Write registers:**

> **x"00000000":**
> > [ 31:24 ] : Transaction ID <br/>
> > [ 23:16 ] : Channel <br/>
> > [ 15:8 ]  : Length <br/>
> > [ 7:0 ]   : Command <br/>
> 
> **x"00000004":**
> > Data (sends start command)
> 
> **x"00000008":**
> > x"00000001"   : Send connect command <br/>
> > x"00000002"   : Send reset command <br/>
> > x"00000003"   : Reset GBT-SC IP
> 

**Read registers:**

> **x"00000008":**
> > [ 31:24 ] : Address (reply) <br/>
> > [ 23:16 ] : Control (reply) <br/>
> > [ 15:8 ]  : Transaction ID (reply) <br/>
> > [ 7:0 ]   : Channel (reply)
> 
> **x"0000000c":**
> > Data (reply)
> 
> **x"00000010":**
> > [ 31:24 ]   : Length (reply) <br/>
> > [ 23:16 ] : Error (reply) <br/>
> > [ 1 ] : Reply received flag

### Software

The software, made to run on the Com Express module of the LHCb's miniDAQ1 setup, is not available yet. However, it includes a low level functions to send commands to the GBT-SCA ASIC as well as a Xilinx Virtual Cable deamon used to test the JTAG interface. Finally, to improve as much as possible the performances of an FPGA configuration through the SCA, an acceleration module has been designed and added into the firmware. The latest measurements reported 15 seconds to configure an Artix 7 FPGA with a bitfile of 2.08 MBytes. A new modification will be made as soon as possible to save a third of this time.

