// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.3 (win64) Build 1682563 Mon Oct 10 19:07:27 MDT 2016
// Date        : Mon Mar 27 14:48:06 2017
// Host        : pcphese57 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               d:/NewGBTRelease/example_designs/xilinx_k7v7/vc707/vivado/vc707_gbt_example_design.srcs/sources_1/ip/tx_sca_dpram/tx_sca_dpram_stub.v
// Design      : tx_sca_dpram
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7vx485tffg1761-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "blk_mem_gen_v8_3_4,Vivado 2016.3" *)
module tx_sca_dpram(clka, ena, wea, addra, dina, clkb, enb, addrb, doutb)
/* synthesis syn_black_box black_box_pad_pin="clka,ena,wea[0:0],addra[6:0],dina[55:0],clkb,enb,addrb[6:0],doutb[55:0]" */;
  input clka;
  input ena;
  input [0:0]wea;
  input [6:0]addra;
  input [55:0]dina;
  input clkb;
  input enb;
  input [6:0]addrb;
  output [55:0]doutb;
endmodule
