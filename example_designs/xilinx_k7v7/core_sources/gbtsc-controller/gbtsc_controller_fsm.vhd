----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.03.2017 15:00:23
-- Design Name: 
-- Module Name: gbtsc_controller_fsm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity gbtsc_controller_fsm is
    Generic(
        MEMORY_DEPTH            : integer   := 128;
        MEMORY_ADDR_MSB         : integer   := 6;
        
        WDT_MAX_VAL             : integer   := 1000000
    );
    Port ( 
    
        -- Clock and reset
        tx_clock                : in std_logic;
        fast_clock              : in std_logic;
        reset_i                 : in std_logic;
        
        -- Control
        start_command_i         : in std_logic;
        tx_wr_i                 : in std_logic;
        tx_wr_data_i            : in std_logic_vector(55 downto 0);
        
        tx_ready_i              : in std_logic;
        rx_reply_received_i     : in std_logic;
        
        -- Tx dpram control
        tx_wr_addr_o            : out std_logic_vector(MEMORY_ADDR_MSB downto 0);
        tx_wr_o                 : out std_logic;
        tx_wr_data_o            : out std_logic_vector(55 downto 0);
        tx_rd_addr_o            : out std_logic_vector(MEMORY_ADDR_MSB downto 0);
        tx_rd_data_o            : in  std_logic_vector(55 downto 0);
        tx_rd_o                 : out std_logic;
        start_command_o         : out std_logic;
        
        tx_address              : out std_logic_vector(7 downto 0);
        tx_transID              : out std_logic_vector(7 downto 0);
        tx_channel              : out std_logic_vector(7 downto 0);
        tx_len                  : out std_logic_vector(7 downto 0);
        tx_command              : out std_logic_vector(7 downto 0);
        tx_data                 : out std_logic_vector(31 downto 0);
        
        -- Status
        delay_cnter_o           : out std_logic_vector(31 downto 0);
        wdt_error_o             : out std_logic;
        ready_o                 : out std_logic    
    );
end gbtsc_controller_fsm;

architecture Behavioral of gbtsc_controller_fsm is
    
    type fsm_t  is (wait_for_start_cmd, wr_pulse, incr_wr_addr, wait_for_gbtsc_ready, wait_for_data_from_dpram, send_command, wait_for_reply);
    signal fsm_state                : fsm_t;
    
    signal tx_wr_addr_s             : integer range 0 to MEMORY_DEPTH;
    signal tx_rd_addr_s             : integer range 0 to MEMORY_DEPTH;
    signal rx_reply_received_s      : std_logic;
    
    signal reset_cnter              : std_logic;
    signal delay_cnter              : integer;
    
begin

    tx_wr_addr_o        <=      std_logic_vector(to_unsigned(tx_wr_addr_s, MEMORY_ADDR_MSB+1));
    tx_rd_addr_o        <=      std_logic_vector(to_unsigned(tx_rd_addr_s, MEMORY_ADDR_MSB+1));
    
    tx_wr_data_o        <= tx_wr_data_i;
                                                            
    tx_address          <=      x"00";
    tx_transID          <=      std_logic_vector(to_unsigned((tx_rd_addr_s+1), 8));
    
    process(tx_clock, reset_i)
    begin
        if reset_i = '1' then
            rx_reply_received_s     <= '0';
        elsif rising_edge(tx_clock) then
            rx_reply_received_s     <= rx_reply_received_i;
        end if;
    end process;
        
    process(tx_clock, reset_i)
        
        variable wdt_cnter          : integer range 0 to WDT_MAX_VAL+1;
        
    begin
    
        if reset_i = '1' then
            tx_wr_addr_s            <= 0;
            tx_rd_addr_s            <= 0;
            
        elsif rising_edge(tx_clock) then
        
            tx_rd_o             <= '0';
            tx_wr_o             <= '0';
            start_command_o     <= '0';
            
            case fsm_state is
            
                when wait_for_start_cmd         =>  ready_o                 <= '1';
                                                    reset_cnter             <= '1';
                
                                                    if start_command_i = '1' then
                                                        ready_o             <= '0';
                                                        tx_rd_addr_s        <= 0;
                                                        fsm_state           <= wait_for_gbtsc_ready;
                                                        
                                                    elsif tx_wr_i = '1' then      
                                                        fsm_state           <= wr_pulse;
                                                        
                                                    end if;
                
                when wr_pulse                   =>  tx_wr_o             <= '1';
                                                    fsm_state           <= incr_wr_addr;
                                                    
                when incr_wr_addr               =>  tx_wr_addr_s        <= tx_wr_addr_s + 1;
                                                    fsm_state           <= wait_for_start_cmd;
                
                when wait_for_gbtsc_ready       =>  reset_cnter             <= '0';
                                                    if tx_ready_i = '1' then
                                                        tx_rd_o             <= '1';
                                                        wdt_cnter           := 0;
                                                        fsm_state           <= send_command; --wait_for_data_from_dpram;
                                                    end if;
                
                when wait_for_data_from_dpram   =>  if wdt_cnter >= 0 then
                                                        fsm_state           <= send_command;
                                                    else
                                                        wdt_cnter           := wdt_cnter + 1;
                                                    end if;
                
                when send_command               =>  tx_channel              <= tx_rd_data_o(55 downto 48);
                                                    tx_len                  <= tx_rd_data_o(47 downto 40);
                                                    tx_command              <= tx_rd_data_o(39 downto 32);
                                                    tx_data                 <= tx_rd_data_o(31 downto 0);
                                                    
                                                    start_command_o         <= '1';
                                                    wdt_cnter               := 0;
                                                    fsm_state               <= wait_for_reply;
                
                when wait_for_reply             =>  wdt_cnter          := wdt_cnter + 1;
                
                                                    if rx_reply_received_s = '1' or wdt_cnter >= WDT_MAX_VAL then
                                                    
                                                        if tx_rd_addr_s >= (tx_wr_addr_s-1) then
                                                            tx_wr_addr_s            <= 0;
                                                            delay_cnter_o           <= std_logic_vector(to_unsigned(delay_cnter,32));
                                                            fsm_state               <= wait_for_start_cmd;
                                                            
                                                        else
                                                            tx_rd_addr_s            <= tx_rd_addr_s + 1;
                                                            fsm_state               <= wait_for_gbtsc_ready;
                                                            
                                                        end if;
                                                    
                                                        if wdt_cnter >= WDT_MAX_VAL then
                                                            wdt_error_o             <= '1';
                                                        end if;
                                                         
                                                    end if;
            
            end case;
            
        end if;
        
    end process;
    
    process(reset_cnter, fast_clock)
    begin
    
        if reset_cnter = '1' then
            delay_cnter     <= 0;
        elsif rising_edge(fast_clock) then
            delay_cnter     <= delay_cnter + 1;
        end if;
        
    end process;
    
end Behavioral;
