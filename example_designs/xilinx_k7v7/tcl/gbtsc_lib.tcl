

proc reset_gbtsc {} {
	
	create_hw_axi_txn wr_gbtsc_reset_command [get_hw_axis hw_axi_1] -address 00000008 -data 00000000 -type write -force
	run_hw_axi wr_gbtsc_reset_command

}

proc send_reset {} {
	
	create_hw_axi_txn wr_start_reset_command [get_hw_axis hw_axi_1] -address 00000008 -data 00000002 -type write -force
	run_hw_axi wr_start_reset_command

}

proc send_connect {} {
	
	create_hw_axi_txn wr_start_connect_command [get_hw_axis hw_axi_1] -address 00000008 -data 00000001 -type write -force
	run_hw_axi wr_start_connect_command

}

proc write_command {channel len command data} {

	set info [expr [expr $command << 24] | [expr $len << 16] | [expr $channel << 8] | 0x00000001]
	set formated_info   [format "0x%08x" $info]
	set formated_data   [format "0x%08x" $data]
	
    set exist [info procs write_register]
    if {$exist == ""} {
        puts "\nPlease source JTAG_lib before sourcing the gbtsc_lib !"
        return
    }

    write_register 0x00000000 $formated_info
    write_register 0x00000004 $formated_data
    
    set reply_received [read_register 0x00000008]
    while { $reply_received != 0x00000001 } {
        set reply_received [read_register 0x00000008]
    }
}

proc read_command {channel len command data} {

	write_command $channel $len $command $data
    
    set rx_data [read_register 0x00000004]
    puts [format "Reply data : 0x%s" $rx_data]
}