proc config_I2C_int0 {} {
	
	write_command 0x00 0x01 0x02 0xFF000000
	
	#I2C channel : 0x03
	
	# Control register:
	#	FREQ[1:0]	: "00" -> 100kHz, "01" -> 200kHz, "10" -> 400kHz, "11" -> 1MHz
	#	NBYTE[6:2]	: Number of bytes to be transmitted (used only for multi-byte transmission)
	#	SCLMODE[7]	: '0' -> Open-drain (Z/GND), '1' -> CMOS (VCC/GND)
	#
	#	Configuration:
	#		Multi-byte mode	: ENABLED 		-> 		3 bytes :	< ADDR >< Register >< Value>
	#		Frequency		: 100kHz
	#		SCL mode		: Open-drain
	#
	#		Value			: 8'b00001000
	write_command 0x03 0x01 0x30 0x00000008
	
	send_queue
	
}

proc MCP23008_write_reg { addr reg value } {

	set start [format "0x%08x" $addr]
	set data [format "0x%08x" [expr [expr $value << 24] | [expr $reg << 16]]]
	
	puts $data
	
	write_command 0x03 0x04 0x40 $data
	write_command 0x03 0x01 0xDA $start
	
	send_queue
	
}