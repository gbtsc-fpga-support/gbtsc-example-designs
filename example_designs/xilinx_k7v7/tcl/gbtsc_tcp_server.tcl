# Copied from Echo_Server example design in Tcl / modif. by EBSM
# LPGBT_SOFT_Server --
#	Open the server listening socket
#	and enter the Tcl event loop
#
# Arguments:
#	port	The server's port number

proc GBTSC_Server {port} {
    global state
    
    set s [socket -server GBTSC_Accept $port]
    vwait state
    close $s
}

# GBTSC_Accept --
#	Accept a connection from a new client.
#	This is called after a new socket connection
#	has been created by Tcl.
#
# Arguments:
#	sock	The new socket connection to the client
#	addr	The client's IP address
#	port	The client's port number
	
proc GBTSC_Accept {sock addr port} {
    global gbtsc

    # Record the client's information

    puts "Accept $sock from $addr port $port"
    set gbtsc(addr,$sock) [list $addr $port]

    # Ensure that each "puts" by the server
    # results in a network transmission

    fconfigure $sock -buffering line

    # Set up a callback for when the client sends data

    fileevent $sock readable [list LPGBT_SOFT $sock]
}

# LPGBT_SOFT --
#	This procedure is called when the server
#	can read data from the client
#
# Arguments:
#	sock	The socket connection to the client

proc LPGBT_SOFT {sock} {
    global gbtsc
    global state
    
    set gpio_rt gpio_rt
    set gpio_wt gpio_wt
	
    # Check end of file or abnormal connection drop,
    # then gbtsc data back to the client.

    if {[eof $sock] || [catch {gets $sock line}]} {
        #close $sock
        #puts "Close $gbtsc(addr,$sock)"
        #unset gbtsc(addr,$sock)
    } else {
		################################ Process Data #################################
		set rcvd_cmd $line
        set ope [lindex $rcvd_cmd 0]
		set addr [lindex $rcvd_cmd 1]
		set data [lindex $rcvd_cmd 2]
		
        switch $ope {
          "r" {
			  #Rd
              create_hw_axi_txn rd_gbtic_reg [get_hw_axis hw_axi_1] -address $addr -type read -force
              run_hw_axi rd_gbtic_reg
            
              set data [get_property DATA [get_hw_axi_txn rd_gbtic_reg]]
              
			  puts $sock $data
              puts [format "Read value (reg. 0x%08x): 0x%08x" $addr $data]
		  }
		  
          "w" {
              #Wr	  
              puts [format "Write value (reg. 0x%08x): 0x%08x" $addr $data]
			  create_hw_axi_txn wr_reg [get_hw_axis hw_axi_1] -address $addr -data $data -type write -force
              run_hw_axi wr_reg
		  }
          "q" {
              puts "Exit"
              set state accepted
          }
        }
		
		################################# End Process Data #################################
		# puts "Finished data processing"
		# puts $sock $line		
		# puts $line
	}
}

#close_hw

#Open Hardware Target (Already connected if you are in Program and debug mode)
#open_hw
#connect_hw_server
#open_hw_target

#set_property PROGRAM.FILE {../vc707/vivado/vc707_gbt_example_design.runs/impl_1/vc707_gbt_example_design.bit} [lindex [get_hw_devices] 0]
#set_property PROBES.FILE {../vc707/vivado/vc707_gbt_example_design.runs/impl_1/debug_nets.ltx} [lindex [get_hw_devices] 0]

#set_property PROGRAM.FILE {../lpgbt/lpgbt.runs/impl_1/system_wrapper.bit} [lindex [get_hw_devices] 0]
#set_property PROBES.FILE  {../lpgbt/lpgbt.runs/impl_1/debug_nets.ltx} [lindex [get_hw_devices] 0]

#current_hw_device [lindex [get_hw_devices] 0]
#refresh_hw_device [lindex [get_hw_devices] 0]

#Create Transactions
#set gpio_rt gpio_rt
#set gpio_wt gpio_wt
#create_hw_axi_txn $gpio_rt [get_hw_axis hw_axi_1] -force -type read -address 0x00000000
#create_hw_axi_txn $gpio_wt [get_hw_axis hw_axi_1] -force -type write -address 0x00000000 -data {00000000}

puts "################# gbtsc - TEST CONTROL ##################"
puts "# Socket Port: 8555                                     #"
puts "# IP Address (localhost): 127.0.0.1                     #"
puts "#########################################################"

GBTSC_Server 8555
