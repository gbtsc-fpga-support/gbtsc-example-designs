# JTAG low level library

proc read_register {addr} {
	
	create_hw_axi_txn rd_gbtic_reg [get_hw_axis hw_axi_1] -address $addr -type read -force
	run_hw_axi rd_gbtic_reg
	
	set rdata_tmp [get_property DATA [get_hw_axi_txn rd_gbtic_reg]]
	return $rdata_tmp
}

proc write_register {addr data} {
	
	create_hw_axi_txn wr_reg [get_hw_axis hw_axi_1] -address $addr -data $data -type write -force
	run_hw_axi wr_reg

}