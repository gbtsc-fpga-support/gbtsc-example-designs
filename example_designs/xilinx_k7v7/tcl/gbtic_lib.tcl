# GBTx configuration (write from file / print configuration)
proc configure_gbtx {GBTAddr BufferSize filepath} {

	set fp [open $filepath r]
	set file_data [read $fp]
	close $fp	
	
	set data [split $file_data "\n"]
	set datasize [llength $data]
	
	for { set i 0}  {$i < $datasize} {set i [expr $i + $nbSent]} {
		set range [lrange $data $i [expr $i + [expr $BufferSize - 1]]]
		set nbSent [llength $range]
		
		write_gbtx_registers $GBTAddr $i $range
	}

}

proc log_gbtx_conf {GBTxaddr BufferSize NumberOfWords filepath loopNumber} {

	if { $NumberOfWords > $BufferSize } {
	
		puts "NumberOfWord > BufferSize: not implemented yet"
		return
        
	} else {
	
		set fo [open $filepath "w"] 
		
		for { set i 0}  {$i < $loopNumber} {incr i} {
		
			puts -nonewline [format "Read %d GBTx (0x%02x) in progress ... " $i $GBTxaddr]
			
			puts -nonewline $fo [format "Read %d GBTx (0x%02x):" $i $GBTxaddr]
			array set a [read_gbtx_registers $GBTxaddr 0 $NumberOfWords]
			array_foreach key a {
				puts -nonewline $fo [format "%s " [string toupper [format "%02x" $a($key)]]]
			}
			
			puts $fo ""
			puts "Done"
			
		}
		
		close $fo
	}
}

proc print_gbtx_registers {GBTxaddr RAMptr NumberOfWords} {

	puts [format "Read GBTx (0x%02x):" $GBTxaddr]
	array set a [read_gbtx_registers $GBTxaddr $RAMptr $NumberOfWords]
    
    puts [format "  Chip address (+R/W) -> %sh " [string toupper [format "%02x" $a(0)]]]
    puts [format "  Register address -> %sh " [string toupper [format "%02x%02x" $a(5) $a(4)]]]
    puts [format "  Read count -> %sh " [string toupper [format "%02x%02x" $a(3) $a(2)]]]
    puts [format "  Parity -> %sh " [string toupper [format "%02x" $a([expr [array size a] - 3])]]]
    
    puts ""
	puts "Data:"
        
    for {set key 6} { $key < [expr [array size a] - 3] } {incr key} {
        puts [format "  reg %d -> %sh " [expr $RAMptr + $key - 6] [string toupper [format "%02x" $a($key)]]]
    }

	#array_foreach key a {
		
	#}
}

# GBTx registers access
proc write_gbtx_registers {GBTx_addr RAM_ptr value} {

    set exist [info procs write_register]
    if {$exist == ""} {
        puts "\nPlease source JTAG_lib before sourcing the gbtsc_lib !"
        return
    }
    
	foreach val $value {
		set regval [format "0x000000%02x" $val]
		write_register 0x00000018 $regval
	}

	set config [format "0x%08x" $RAM_ptr]
	write_register 0x00000014 $config

	set command [format "0x000002%02x" $GBTx_addr]
	write_register 0x00000010 $command
	
}

proc read_gbtx_registers {GBTxaddr RAMptr NumberOfWords} {

    set exist [info procs write_register]
    if {$exist == ""} {
        puts "\nPlease source JTAG_lib before sourcing the gbtsc_lib !"
        return
    }
    
	# Configure IP	
	set conf [format "%04x%04x" $NumberOfWords $RAMptr]
	write_register 0x00000014 $conf
	
	# Send read command to GBTx
	write_register 0x00000010 [format "0x000001%02x" $GBTxaddr]
	
	set status [read_register 0x00000018]
	set ic_emtpy [expr $status & 0x00000001]
	
	while {$ic_emtpy == 0x01} {
		set status [read_register 0x00000018]
		set ic_emtpy [expr $status & 0x00000001]
	}
	
	set mem_addr [read_register 0x00000014]
	set mem_addr [expr 0x$mem_addr & 0x0000FFFF]
	
	# Read dummy data => send rd pulse
	read_register 0x0000001C
	
	array set registers {}	
	while {$ic_emtpy != 0x01} {
				
		set status [read_register 0x00000018]
		set ic_emtpy [expr $status & 0x00000001]
		
		set mem_val [read_register 0x0000001C]
		set mem_val [expr 0x$mem_val & 0x000000FF]
		
		array_add registers $mem_addr $mem_val
		
		incr mem_addr
	}
	
	return [array get registers]
}

# Tools
proc array_add {ary_name key value} {
    upvar 1 $ary_name ary
    set ary($key) $value
    lappend ary() $key
}

proc array_foreach {var_name ary_name script} {
    upvar 1 $var_name var
    upvar 1 $ary_name ary
    foreach var $ary() {
        uplevel 1 $script
    }
}