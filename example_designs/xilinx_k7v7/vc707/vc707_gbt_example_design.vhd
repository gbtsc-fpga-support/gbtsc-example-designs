--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               CERN (PH-ESE-BE)                                                         
-- Engineer:              Manoel Barros Marin (manoel.barros.marin@cern.ch) (m.barros.marin@ieee.org)
--                                                                                                 
-- Project Name:          GBT-FPGA                                                                
-- Module Name:           VC707 - GBT Bank example design                                        
--                                                                                                 
-- Language:              VHDL'93                                                                  
--                                                                                                   
-- Target Device:         VC707 (Xilinx Virtex 7)                                                         
-- Tool version:          ISE 14.5                                                                
--                                                                                                   
-- Version:               3.0                                                                      
--
-- Description:            
--
-- Versions history:      DATE         VERSION   AUTHOR            DESCRIPTION
--
--                        06/03/2013   3.0       M. Barros Marin   First .vhd module definition           
--
-- Additional Comments:   Note!! Only ONE GBT Bank with ONE link can be used in this example design.     
--
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- !!                                                                                           !!
-- !! * The different parameters of the GBT Bank are set through:                               !!  
-- !!   (Note!! These parameters are vendor specific)                                           !!                    
-- !!                                                                                           !!
-- !!   - The MGT control ports of the GBT Bank module (these ports are listed in the records   !!
-- !!     of the file "<vendor>_<device>_gbt_bank_package.vhd").                                !! 
-- !!     (e.g. xlx_v6_gbt_bank_package.vhd)                                                    !!
-- !!                                                                                           !!  
-- !!   - By modifying the content of the file "<vendor>_<device>_gbt_bank_user_setup.vhd".     !!
-- !!     (e.g. xlx_v6_gbt_bank_user_setup.vhd)                                                 !! 
-- !!                                                                                           !! 
-- !! * The "<vendor>_<device>_gbt_bank_user_setup.vhd" is the only file of the GBT Bank that   !!
-- !!   may be modified by the user. The rest of the files MUST be used as is.                  !!
-- !!                                                                                           !!  
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--                                                                                              
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_banks_user_setup.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity vc707_gbt_example_design is   
   port (   
      
      --===============--     
      -- General reset --     
      --===============--     

      CPU_RESET                                      : in  std_logic;     
      
      --===============--
      -- Clocks scheme --
      --===============-- 
      
      -- Fabric clock:
      ----------------     

      USER_CLOCK_P                                   : in  std_logic;
      USER_CLOCK_N                                   : in  std_logic;      
      
      -- MGT(GTX) reference clock:
      ----------------------------
      
      -- Comment: * The MGT reference clock MUST be provided by an external clock generator.
      --
      --          * The MGT reference clock frequency must be 120MHz for the latency-optimized GBT Bank.      
      
      SMA_MGT_REFCLK_P                               : in  std_logic;
      SMA_MGT_REFCLK_N                               : in  std_logic; 
      
      --==========--
      -- MGT(GTX) --
      --==========--                   
      
      -- Serial lanes:
      ----------------
      
      SFP_TX_P                                       : out std_logic;
      SFP_TX_N                                       : out std_logic;
      SFP_RX_P                                       : in  std_logic;
      SFP_RX_N                                       : in  std_logic;                  
      
      -- SFP control:
      ---------------
      
      SFP_TX_DISABLE                                 : out std_logic;
      
      --===============--      
      -- On-board LEDs --      
      --===============--

      GPIO_LED_0_LS                                  : out std_logic;
      GPIO_LED_1_LS                                  : out std_logic;
      GPIO_LED_2_LS                                  : out std_logic;
      GPIO_LED_3_LS                                  : out std_logic;
      GPIO_LED_4_LS                                  : out std_logic;
      GPIO_LED_5_LS                                  : out std_logic;
      GPIO_LED_6_LS                                  : out std_logic;
      GPIO_LED_7_LS                                  : out std_logic;      
      
      --====================--
      -- Signals forwarding --
      --====================--
      
      -- SMA output:
      --------------
      
      -- Comment: USER_SMA_GPIO_P is connected to a multiplexor that switches between TX_FRAMECLK and TX_WORDCLK.
      
      USER_SMA_GPIO_P                                : out std_logic;        
      
      -- Pattern match flags:
      -----------------------
      
      FMC1_HPC_LA00_CC_P                             : out std_logic;       
      FMC1_HPC_LA01_CC_P                             : out std_logic;
      
      -- Clocks forwarding:
      ---------------------  
      
      -- Comment: * FMC1_HPC_LA02_P and FMC1_HPC_LA03_P are used for forwarding TX_FRAMECLK and TX_WORDCLK respectively.
      --      
      --          * FMC1_HPC_LA04_P and FMC1_HPC_LA05_P are used for forwarding RX_FRAMECLK and RX_WORDCLK respectively.
      
      FMC1_HPC_LA02_P                                : out std_logic; 
      FMC1_HPC_LA03_P                                : out std_logic; 
      -----------------------------------------------
      FMC1_HPC_LA04_P                                : out std_logic; 
      FMC1_HPC_LA05_P                                : out std_logic 

   );
end vc707_gbt_example_design;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture structural of vc707_gbt_example_design is
   
   --================================ Signal Declarations ================================--          
   
   --===============--     
   -- General reset --     
   --===============--     

   signal reset_from_genRst                          : std_logic;    
   
   --===============--
   -- Clocks scheme -- 
   --===============--   
   
   -- Fabric clock:
   ----------------
   
   signal fabricClk_from_userClockIbufgds            : std_logic;     

   -- MGT(GTX) reference clock:     
   ----------------------------     
  
   signal mgtRefClk_from_smaMgtRefClkIbufdsGtxe2     : std_logic;   

    -- Frame clock:
    ---------------
    signal txFrameClk_from_txPll                     : std_logic;
    
   --================--
   -- Clock component--
   --================--
   -- Vivado synthesis tool does not support mixed-language
   -- Solution: http://www.xilinx.com/support/answers/47454.html
   COMPONENT xlx_k7v7_tx_pll PORT(
      clk_in1: in std_logic;
      RESET: in std_logic;
      CLK_OUT1: out std_logic;
      LOCKED: out std_logic
   );
   END COMPONENT;
   
   --=========================--
   -- GBT Bank example design --
   --=========================--
   
   -- Control:
   -----------
   
   signal generalReset_from_user                     : std_logic;      
   signal manualResetTx_from_user                    : std_logic; 
   signal manualResetRx_from_user                    : std_logic; 
   signal clkMuxSel_from_user                        : std_logic;       
   signal testPatterSel_from_user                    : std_logic_vector(1 downto 0); 
   signal loopBack_from_user                         : std_logic_vector(2 downto 0); 
   signal resetDataErrorSeenFlag_from_user           : std_logic; 
   signal resetGbtRxReadyLostFlag_from_user          : std_logic; 
   signal txIsDataSel_from_user                      : std_logic;   
   --------------------------------------------------      
   signal latOptGbtBankTx_from_gbtExmplDsgn          : std_logic;
   signal latOptGbtBankRx_from_gbtExmplDsgn          : std_logic;
   signal txFrameClkPllLocked_from_gbtExmplDsgn      : std_logic;
   signal mgtReady_from_gbtExmplDsgn                 : std_logic; 
   signal rxBitSlipNbr_from_gbtExmplDsgn             : std_logic_vector(GBTRX_BITSLIP_NBR_MSB downto 0);
   signal rxWordClkReady_from_gbtExmplDsgn           : std_logic; 
   signal rxFrameClkReady_from_gbtExmplDsgn          : std_logic; 
   signal gbtRxReady_from_gbtExmplDsgn               : std_logic;    
   signal rxIsData_from_gbtExmplDsgn                 : std_logic;        
   signal gbtRxReadyLostFlag_from_gbtExmplDsgn       : std_logic; 
   signal rxDataErrorSeen_from_gbtExmplDsgn          : std_logic; 
   signal rxExtrDataWidebusErSeen_from_gbtExmplDsgn  : std_logic; 
   
   -- Data:
   --------
   
   signal txData_to_gbtExmplDsgn                     : std_logic_vector(83 downto 0);
   signal txData_from_gbtExmplDsgn                   : std_logic_vector(83 downto 0);
   signal rxData_from_gbtExmplDsgn                   : std_logic_vector(83 downto 0);
   --------------------------------------------------      
   signal txExtraDataWidebus_from_gbtExmplDsgn       : std_logic_vector(31 downto 0);
   signal rxExtraDataWidebus_from_gbtExmplDsgn       : std_logic_vector(31 downto 0);
   
   
    --SCA debug
    -----------
    signal reset_sca: std_logic;
    signal sca_ready: std_logic;
    signal start_sca: std_logic;
    signal start_reset: std_logic;
    signal start_connect: std_logic;
    
    signal sca_rx_parr: std_logic_vector(159 downto 0);
    signal sca_rx_done: std_logic;
    
    signal ec_line_tx  : std_logic_vector(1 downto 0);
    signal ec_line_rx  : std_logic_vector(1 downto 0);
    
    signal tx_ready: std_logic;
    signal tx_addr: std_logic_vector(7 downto 0);
    signal tx_ctrl: std_logic_vector(7 downto 0);
    signal tx_trid: std_logic_vector(7 downto 0);
    signal tx_ch: std_logic_vector(7 downto 0);
    signal tx_len: std_logic_vector(7 downto 0);
    signal tx_cmd: std_logic_vector(7 downto 0);
    signal tx_data: std_logic_vector(31 downto 0);
    
    signal rx_addr: std_logic_vector(7 downto 0);
    signal rx_ctrl: std_logic_vector(7 downto 0);
    signal rx_trid: std_logic_vector(7 downto 0);
    signal rx_ch: std_logic_vector(7 downto 0);
    signal rx_len: std_logic_vector(7 downto 0);
    signal rx_err: std_logic_vector(7 downto 0);
    signal rx_data: std_logic_vector(31 downto 0);
    
    signal m_axi_awaddr 		:  STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m_axi_awprot         :  STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal m_axi_awvalid         :  STD_LOGIC;
    signal m_axi_awready         :  STD_LOGIC;
    signal m_axi_wdata         :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m_axi_wstrb         :  STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal m_axi_wvalid         :  STD_LOGIC;
    signal m_axi_wready         :  STD_LOGIC;
    signal m_axi_bresp         :  STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal m_axi_bvalid         :  STD_LOGIC;
    signal m_axi_bready         :  STD_LOGIC;
    signal m_axi_araddr         :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m_axi_arprot         :  STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal m_axi_arvalid         :  STD_LOGIC;
    signal m_axi_arready         :  STD_LOGIC;
    signal m_axi_rdata         :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m_axi_rresp         :  STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal m_axi_rvalid         :  STD_LOGIC;
    signal m_axi_rready         :  STD_LOGIC;
    
    signal m_axi_awaddr_to_gbtic : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m_axi_awprot_to_gbtic : STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal m_axi_awvalid_to_gbtic : STD_LOGIC;
    signal m_axi_awready_to_gbtic : STD_LOGIC;
    signal m_axi_wdata_to_gbtic : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m_axi_wstrb_to_gbtic : STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal m_axi_wvalid_to_gbtic : STD_LOGIC;
    signal m_axi_wready_to_gbtic : STD_LOGIC;
    signal m_axi_bresp_to_gbtic : STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal m_axi_bvalid_to_gbtic : STD_LOGIC;
    signal m_axi_bready_to_gbtic : STD_LOGIC;
    signal m_axi_araddr_to_gbtic : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m_axi_arprot_to_gbtic : STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal m_axi_arvalid_to_gbtic : STD_LOGIC;
    signal m_axi_arready_to_gbtic : STD_LOGIC;
    signal m_axi_rdata_to_gbtic : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m_axi_rresp_to_gbtic : STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal m_axi_rvalid_to_gbtic : STD_LOGIC;
    signal m_axi_rready_to_gbtic : STD_LOGIC;
    
    signal m_axi_awaddr_to_gbtsc : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m_axi_awprot_to_gbtsc : STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal m_axi_awvalid_to_gbtsc : STD_LOGIC;
    signal m_axi_awready_to_gbtsc : STD_LOGIC;
    signal m_axi_wdata_to_gbtsc : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m_axi_wstrb_to_gbtsc : STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal m_axi_wvalid_to_gbtsc : STD_LOGIC;
    signal m_axi_wready_to_gbtsc : STD_LOGIC;
    signal m_axi_bresp_to_gbtsc : STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal m_axi_bvalid_to_gbtsc : STD_LOGIC;
    signal m_axi_bready_to_gbtsc : STD_LOGIC;
    signal m_axi_araddr_to_gbtsc : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m_axi_arprot_to_gbtsc : STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal m_axi_arvalid_to_gbtsc : STD_LOGIC;
    signal m_axi_arready_to_gbtsc : STD_LOGIC;
    signal m_axi_rdata_to_gbtsc : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m_axi_rresp_to_gbtsc : STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal m_axi_rvalid_to_gbtsc : STD_LOGIC;
    signal m_axi_rready_to_gbtsc : STD_LOGIC;
            
    signal cmd_delay            : std_logic_vector(31 downto 0);
    
    COMPONENT debug_jtag_master
      PORT (
        aclk : IN STD_LOGIC;
        aresetn : IN STD_LOGIC;
        m_axi_awaddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_awvalid : OUT STD_LOGIC;
        m_axi_awready : IN STD_LOGIC;
        m_axi_wdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_wstrb : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_wvalid : OUT STD_LOGIC;
        m_axi_wready : IN STD_LOGIC;
        m_axi_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_bvalid : IN STD_LOGIC;
        m_axi_bready : OUT STD_LOGIC;
        m_axi_araddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_arvalid : OUT STD_LOGIC;
        m_axi_arready : IN STD_LOGIC;
        m_axi_rdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_rvalid : IN STD_LOGIC;
        m_axi_rready : OUT STD_LOGIC
      );
    END COMPONENT;   
       
    -- IC Debug
    ------------
    signal ic_ready                   : std_logic;
    signal ic_empty                   : std_logic;
    signal GBTx_address_to_gbtic      : std_logic_vector(7 downto 0);
    signal Register_addr_to_gbtic     : std_logic_vector(15 downto 0);
    signal nb_to_be_read_to_gbtic     : std_logic_vector(15 downto 0);
    signal start_write_to_gbtic       : std_logic;
    signal start_read_to_gbtic        : std_logic;
    signal data_to_gbtic              : std_logic_vector(7 downto 0);
    signal wr_to_gbtic                : std_logic;
    signal data_from_gbtic            : std_logic_vector(7 downto 0);
    signal rd_to_gbtic                : std_logic;
    
    signal GBTx_address         : std_logic_vector(7 downto 0);
    signal Register_ptr         : std_logic_vector(15 downto 0);
    signal ic_start_wr          : std_logic;
    signal ic_wr                : std_logic;
    signal ic_data_wr           : std_logic_vector(7 downto 0);
    signal ic_start_rd          : std_logic;
    signal ic_nb_to_be_read_rd  : std_logic_vector(15 downto 0);
    signal ic_empty_rd          : std_logic;
    signal ic_rd                : std_logic;
    signal ic_data_rd           : std_logic_vector(7 downto 0);
    
    signal GBTx_rd_addr         : std_logic_vector(7 downto 0);
    signal GBTx_rd_mem_ptr      : std_logic_vector(15 downto 0);
    signal GBTx_rd_mem_size     : std_logic_vector(15 downto 0);
    
   --===========--
   -- Chipscope --
   --===========--
   
   signal vioControl_from_icon                       : std_logic_vector(35 downto 0); 
   signal txIlaControl_from_icon                     : std_logic_vector(35 downto 0); 
   signal rxIlaControl_from_icon                     : std_logic_vector(35 downto 0); 
   --------------------------------------------------
   signal sync_from_vio                              : std_logic_vector(11 downto 0);
   signal async_to_vio                               : std_logic_vector(17 downto 0);
   
   --=====================--
   -- Latency measurement --
   --=====================--
   
   signal txFrameClk_from_gbtExmplDsgn               : std_logic;
   signal txWordClk_from_gbtExmplDsgn                : std_logic;
   signal rxFrameClk_from_gbtExmplDsgn               : std_logic;
   signal rxWordClk_from_gbtExmplDsgn                : std_logic;
   --------------------------------------------------                                    
   signal txMatchFlag_from_gbtExmplDsgn              : std_logic;
   signal rxMatchFlag_from_gbtExmplDsgn              : std_logic;
   
     -- ILA component  --
     --================--
     -- Vivado synthesis tool does not support mixed-language
     -- Solution: http://www.xilinx.com/support/answers/47454.html
     COMPONENT xlx_k7v7_vivado_debug 
       PORT(
        CLK: in std_logic;
        PROBE0: in std_logic_vector(83 downto 0);
        PROBE1: in std_logic_vector(31 downto 0);
        PROBE2: in std_logic_vector(3 downto 0);
        PROBE3: in std_logic_vector(0 downto 0)
     );
     END COMPONENT;
     
     
     COMPONENT xlx_k7v7_vio
       PORT (
         clk : IN STD_LOGIC;
         probe_in0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_in1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_in2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_in3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_in4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_in5 : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
         probe_in6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_in7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_in8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_in9 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_in10 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_in11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_in12 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_out0 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_out1 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_out2 : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
         probe_out3 : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
         probe_out4 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_out5 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_out6 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_out7 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
         probe_out8 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
       );
     END COMPONENT;
                      
          
     signal sysclk  : std_logic;
     
     attribute mark_debug : string;
     
     attribute mark_debug of rxIsData_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of txFrameClkPllLocked_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of latOptGbtBankTx_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of mgtReady_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of rxWordClkReady_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of rxBitSlipNbr_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of rxFrameClkReady_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of gbtRxReady_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of gbtRxReadyLostFlag_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of rxDataErrorSeen_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of rxExtrDataWidebusErSeen_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of latOptGbtBankRx_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of generalReset_from_user: signal is "TRUE";
     attribute mark_debug of clkMuxSel_from_user: signal is "TRUE";
     attribute mark_debug of testPatterSel_from_user: signal is "TRUE";
     attribute mark_debug of loopBack_from_user: signal is "TRUE";
     attribute mark_debug of resetDataErrorSeenFlag_from_user: signal is "TRUE";
     attribute mark_debug of resetGbtRxReadyLostFlag_from_user: signal is "TRUE";
     attribute mark_debug of txIsDataSel_from_user: signal is "TRUE";
     attribute mark_debug of manualResetTx_from_user: signal is "TRUE";
     attribute mark_debug of manualResetRx_from_user: signal is "TRUE";
   --=====================================================================================--   

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--
   
   --==================================== User Logic =====================================--
   
   --=============--
   -- SFP control -- 
   --=============-- 
   
   SFP_TX_DISABLE                                    <= '0';   
   
   --===============--
   -- General reset -- 
   --===============--
   
   genRst: entity work.xlx_k7v7_reset
      generic map (
         CLK_FREQ                                    => 156e6)
      port map (     
         CLK_I                                       => fabricClk_from_userClockIbufgds,
         RESET1_B_I                                  => not CPU_RESET, 
         RESET2_B_I                                  => not generalReset_from_user,
         RESET_O                                     => reset_from_genRst 
      ); 

   --===============--
   -- Clocks scheme -- 
   --===============--   
   
   -- Fabric clock:
   ----------------
   
   -- Comment: USER_CLOCK frequency: 156MHz 
   
   userClockIbufgds: ibufgds
      generic map (
         IBUF_LOW_PWR                                => FALSE,      
         IOSTANDARD                                  => "LVDS_25")
      port map (     
         O                                           => fabricClk_from_userClockIbufgds,   
         I                                           => USER_CLOCK_P,  
         IB                                          => USER_CLOCK_N 
      );
   
   -- MGT(GTX) reference clock:
   ----------------------------
   
   -- Comment: * The MGT reference clock MUST be provided by an external clock generator.
   --
   --          * The MGT reference clock frequency must be 120MHz for the latency-optimized GBT Bank. 
   
   smaMgtRefClkIbufdsGtxe2: ibufds_gte2
      port map (
         O                                           => mgtRefClk_from_smaMgtRefClkIbufdsGtxe2,
         ODIV2                                       => open,
         CEB                                         => '0',
         I                                           => SMA_MGT_REFCLK_P,
         IB                                          => SMA_MGT_REFCLK_N
      );

   -- Frame clock
   txPll: xlx_k7v7_tx_pll
      port map (
        clk_in1                                  => mgtRefClk_from_smaMgtRefClkIbufdsGtxe2,
        CLK_OUT1                                 => txFrameClk_from_txPll,
        -----------------------------------------  
        RESET                                    => '0',
        LOCKED                                   => txFrameClkPllLocked_from_gbtExmplDsgn
      );
        
   --=========================--
   -- GBT Bank example design --
   --=========================--
   jtag_master_inst : debug_jtag_master
     PORT MAP (
       aclk => txFrameClk_from_txPll,
       aresetn => txFrameClkPllLocked_from_gbtExmplDsgn,
       m_axi_awaddr => m_axi_awaddr,
       m_axi_awprot => m_axi_awprot,
       m_axi_awvalid => m_axi_awvalid,
       m_axi_awready => m_axi_awready,
       m_axi_wdata => m_axi_wdata,
       m_axi_wstrb => m_axi_wstrb,
       m_axi_wvalid => m_axi_wvalid,
       m_axi_wready => m_axi_wready,
       m_axi_bresp => m_axi_bresp,
       m_axi_bvalid => m_axi_bvalid,
       m_axi_bready => m_axi_bready,
       m_axi_araddr => m_axi_araddr,
       m_axi_arprot => m_axi_arprot,
       m_axi_arvalid => m_axi_arvalid,
       m_axi_arready => m_axi_arready,
       m_axi_rdata => m_axi_rdata,
       m_axi_rresp => m_axi_rresp,
       m_axi_rvalid => m_axi_rvalid,
       m_axi_rready => m_axi_rready
     );
     
   gbtsc_controller_inst: entity work.gbtsc_controller
       Port map( 
           -- AXI4LITE Interface
           tx_clock              => txFrameClk_from_txPll,
           
           -- AXI4LITE Interface
           S_AXI_ARESETN         => txFrameClkPllLocked_from_gbtExmplDsgn,
           S_AXI_AWADDR          => m_axi_awaddr(4 downto 0),
           S_AXI_AWVALID         => m_axi_awvalid,
           S_AXI_AWREADY         => m_axi_awready,
           S_AXI_WDATA           => m_axi_wdata,
           S_AXI_WSTRB           => m_axi_wstrb,
           S_AXI_WVALID          => m_axi_wvalid,
           S_AXI_WREADY          => m_axi_wready,
           S_AXI_BRESP           => m_axi_bresp,
           S_AXI_BVALID          => m_axi_bvalid,
           S_AXI_BREADY          => m_axi_bready,
           S_AXI_ARADDR          => m_axi_araddr(4 downto 0),
           S_AXI_ARVALID         => m_axi_arvalid,
           S_AXI_ARREADY         => m_axi_arready,
           S_AXI_RDATA           => m_axi_rdata,
           S_AXI_RRESP           => m_axi_rresp,
           S_AXI_RVALID          => m_axi_rvalid,
           S_AXI_RREADY          => m_axi_rready,
           
           -- To GBT-SC
           reset_gbtsc         => reset_sca,
           start_reset         => start_reset,
           start_connect       => start_connect,
           start_command       => start_sca,           
                   
           tx_address          => tx_addr,
           tx_transID          => tx_trid,
           tx_channel          => tx_ch,
           tx_len              => tx_len,
           tx_command          => tx_cmd,
           tx_data             => tx_data,           
           
           rx_reply_received_i => sca_rx_done,
           rx_address          => rx_addr,
           rx_transID          => rx_trid,
           rx_channel          => rx_ch,
           rx_len              => rx_len,
           rx_error            => rx_err,
           rx_data             => rx_data,
           
                          
          -- Data to GBT-IC State machine        
          ic_ready             => ic_ready,
          ic_empty             => ic_empty,
          
          -- Configuration
          GBTx_address         => GBTx_address_to_gbtic,
          Register_addr        => Register_addr_to_gbtic,
          nb_to_be_read        => nb_to_be_read_to_gbtic,
          
          -- Control        
          start_write          => start_write_to_gbtic,
          start_read           => start_read_to_gbtic,
          
          -- WRITE register(s)
          data_o               => data_to_gbtic,
          wr                   => wr_to_gbtic,
          
          -- READ register(s)        
          data_i               => data_from_gbtic,
          rd                   => rd_to_gbtic,
                            
           -- Status
           delay_cnter_o       => cmd_delay,
           wdt_error_o         => open,
           ready_o             => open
       );
   
   gbtsc_inst: entity work.gbtsc_top
   generic map(       
       -- IC configuration
       g_IC_FIFO_DEPTH     => 300, --! Defines the depth of the FIFO used to handle the Internal control (Max. number of words/bytes can be read/write from/to a GBTx)
       g_ToLpGBT           => 0,   --! 1 to use LpGBT. Otherwise, it should be 0

       -- EC configuration
       g_SCA_COUNT         => 1               
   )
   port map(
       -- Clock & reset
       tx_clk_i                => txFrameClk_from_txPll,
       tx_clk_en               => '1',
       
       rx_clk_i                => txFrameClk_from_txPll,
       rx_clk_en               => '1',
       
       rx_reset_i              => reset_sca,
       tx_reset_i              => reset_sca,
       
       -- IC configuration        
       tx_GBTx_address_i       => GBTx_address_to_gbtic,
       tx_register_addr_i      => Register_addr_to_gbtic,
       tx_nb_to_be_read_i      => nb_to_be_read_to_gbtic,
       
       -- IC Status
       tx_ready_o              => ic_ready,
       rx_empty_o              => ic_empty,
                  
       -- IC FIFO control
       wr_clk_i                => txFrameClk_from_txPll,
       tx_wr_i                 => wr_to_gbtic,
       tx_data_to_gbtx_i       => data_to_gbtic,
       
       rd_clk_i                => txFrameClk_from_txPll,
       rx_rd_i                 => rd_to_gbtic,
       rx_data_from_gbtx_o     => data_from_gbtic,
       
       -- IC control
       tx_start_write_i        => start_write_to_gbtic,
       tx_start_read_i         => start_read_to_gbtic,
           
       -- SCA control
       sca_enable_i            => "1",
       start_reset_cmd_i       => start_reset,
       start_connect_cmd_i     => start_connect,
       start_command_i         => start_sca,
       inject_crc_error        => '0',
       
       -- SCA command
       tx_address_i            => tx_addr,
       tx_transID_i            => tx_trid,
       tx_channel_i            => tx_ch,
       tx_command_i            => tx_cmd,
       tx_data_i               => tx_data,
       
       rx_received_o(0)        => sca_rx_done,
       rx_address_o(0)         => rx_addr,
       rx_control_o(0)         => open,
       rx_transID_o(0)         => rx_trid,
       rx_channel_o(0)         => rx_ch,
       rx_len_o(0)             => rx_len,
       rx_error_o(0)           => rx_err,
       rx_data_o(0)            => rx_data,

       -- EC line
       ec_data_o(0)            => txData_to_gbtExmplDsgn(81 downto 80),
       ec_data_i(0)            => rxData_from_gbtExmplDsgn(81 downto 80),
       
       -- IC lines
       ic_data_o               => txData_to_gbtExmplDsgn(83 downto 82),
       ic_data_i               => rxData_from_gbtExmplDsgn(83 downto 82)
       
   );
    	
  gbtExmplDsgn_inst: entity work.xlx_k7v7_gbt_example_design
      generic map(
          GBT_BANK_ID                                            => 1,
          NUM_LINKS                                              => GBT_BANKS_USER_SETUP(1).NUM_LINKS,
          TX_OPTIMIZATION                                        => GBT_BANKS_USER_SETUP(1).TX_OPTIMIZATION,
          RX_OPTIMIZATION                                        => GBT_BANKS_USER_SETUP(1).RX_OPTIMIZATION,
          TX_ENCODING                                            => GBT_BANKS_USER_SETUP(1).TX_ENCODING,
          RX_ENCODING                                            => GBT_BANKS_USER_SETUP(1).RX_ENCODING
      )
      port map (
    
          --==============--
          -- Clocks       --
          --==============--
          FRAMECLK_40MHZ                                             => txFrameClk_from_txPll,
          XCVRCLK                                                    => mgtRefClk_from_smaMgtRefClkIbufdsGtxe2,
          
          TX_FRAMECLK_O(1)                                           => txFrameClk_from_gbtExmplDsgn,        
          TX_WORDCLK_O(1)                                            => txWordClk_from_gbtExmplDsgn,          
          RX_FRAMECLK_O(1)                                           => rxFrameClk_from_gbtExmplDsgn,         
          RX_WORDCLK_O(1)                                            => rxWordClk_from_gbtExmplDsgn,      
          
          RX_WORDCLK_RDY_O(1)                                        => rxWordClkReady_from_gbtExmplDsgn,
          RX_FRAMECLK_RDY_O(1)                                       => rxFrameClkReady_from_gbtExmplDsgn,
          
          --==============--
          -- Reset        --
          --==============--
          GBTBANK_GENERAL_RESET_I                                    => reset_from_genRst,
          GBTBANK_MANUAL_RESET_TX_I                                  => manualResetTx_from_user,
          GBTBANK_MANUAL_RESET_RX_I                                  => manualResetRx_from_user,
          
          --==============--
          -- Serial lanes --
          --==============--
          GBTBANK_MGT_RX_P(1)                                        => SFP_RX_P,
          GBTBANK_MGT_RX_N(1)                                        => SFP_RX_N,
          GBTBANK_MGT_TX_P(1)                                        => SFP_TX_P,
          GBTBANK_MGT_TX_N(1)                                        => SFP_TX_N,
          
          --==============--
          -- Data             --
          --==============--        
          GBTBANK_GBT_DATA_I(1)                                      => txData_to_gbtExmplDsgn,
          GBTBANK_WB_DATA_I(1)                                       => (others => '0'),
          
          TX_DATA_O(1)                                               => txData_from_gbtExmplDsgn,            
          WB_DATA_O(1)                                               => open, --txExtraDataWidebus_from_gbtExmplDsgn,
          
          GBTBANK_GBT_DATA_O(1)                                      => rxData_from_gbtExmplDsgn,
          GBTBANK_WB_DATA_O(1)                                       => open, --rxExtraDataWidebus_from_gbtExmplDsgn,
          
          --==============--
          -- Reconf.         --
          --==============--
          GBTBANK_MGT_DRP_RST                                        => '0',
          GBTBANK_MGT_DRP_CLK                                        => fabricClk_from_userClockIbufgds, --
          
          --==============--
          -- TX ctrl        --
          --==============--
          GBTBANK_TX_ISDATA_SEL_I(1)                                => txIsDataSel_from_user, --
          GBTBANK_TEST_PATTERN_SEL_I                                => "11", --testPatterSel_from_user, --
          
          --==============--
          -- RX ctrl      --
          --==============--
          GBTBANK_RESET_GBTRXREADY_LOST_FLAG_I(1)                   => resetGbtRxReadyLostFlag_from_user, --
          GBTBANK_RESET_DATA_ERRORSEEN_FLAG_I(1)                    => resetDataErrorSeenFlag_from_user, --
          
          --==============--
          -- TX Status    --
          --==============--
          GBTBANK_LINK_READY_O(1)                                   => mgtReady_from_gbtExmplDsgn, --
          GBTBANK_TX_MATCHFLAG_O                                    => txMatchFlag_from_gbtExmplDsgn,--
          
          --==============--
          -- RX Status    --
          --==============--
          GBTBANK_GBTRX_READY_O(1)                                  => gbtRxReady_from_gbtExmplDsgn, --
          GBTBANK_LINK1_BITSLIP_O                                   => rxBitSlipNbr_from_gbtExmplDsgn, --
          GBTBANK_GBTRXREADY_LOST_FLAG_O(1)                         => gbtRxReadyLostFlag_from_gbtExmplDsgn, --
          GBTBANK_RXDATA_ERRORSEEN_FLAG_O(1)                        => rxDataErrorSeen_from_gbtExmplDsgn, --
          GBTBANK_RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O(1)           => rxExtrDataWidebusErSeen_from_gbtExmplDsgn, --
          GBTBANK_RX_MATCHFLAG_O(1)                                 => rxMatchFlag_from_gbtExmplDsgn, --
          GBTBANK_RX_ISDATA_SEL_O(1)                                => rxIsData_from_gbtExmplDsgn, --
          GBTBANK_RX_ERRORDETECTED_O                                => open,
          
          --==============--
          -- XCVR ctrl    --
          --==============--
          GBTBANK_LOOPBACK_I                                        => loopBack_from_user, --
          
          GBTBANK_TX_POL(1)                                         => '0', --
          GBTBANK_RX_POL(1)                                         => '0' --
     );                                   
   
   --==============--   
   -- Test control --   
   --==============--
   
   -- Chipscope:
   -------------   
   
   -- Comment: * Chipscope is used to control the example design as well as for transmitted and received data analysis.
   --
   --          * Note!! TX and RX DATA do not share the same ILA module (txIla and rxIla respectively) 
   --            because when receiving RX DATA from another board with a different reference clock, the 
   --            TX_FRAMECLK/TX_WORDCLK domains are asynchronous with respect to the RX_FRAMECLK/RX_WORDCLK domains.        
   --
   --          * After FPGA configuration using Chipscope, open the project "vc707_gbt_example_design.cpj" 
   --            that can be found in:
   --            "..\example_designs\xilix_k7v7\vc707\chipscope_project\".  
       
   debugclk_bufg: BUFG 
      port map (
         I                                           => txFrameClk_from_txPll,
         O                                           => sysclk
      );
   
  latOptGbtBankTx_from_gbtExmplDsgn                       <= '1' when GBT_BANKS_USER_SETUP(1).TX_OPTIMIZATION = LATENCY_OPTIMIZED else
                                                                         '0';
  latOptGbtBankRx_from_gbtExmplDsgn                       <= '1' when GBT_BANKS_USER_SETUP(1).RX_OPTIMIZATION = LATENCY_OPTIMIZED else
                                                                         '0';
                                                                                              
   vio : xlx_k7v7_vio
         PORT MAP (
           clk => sysclk,
           probe_in0(0) => rxIsData_from_gbtExmplDsgn,
           probe_in1(0) => txFrameClkPllLocked_from_gbtExmplDsgn,
           probe_in2(0) => latOptGbtBankTx_from_gbtExmplDsgn,
           probe_in3(0) => mgtReady_from_gbtExmplDsgn,
           probe_in4(0) => rxWordClkReady_from_gbtExmplDsgn,
           probe_in5    => rxBitSlipNbr_from_gbtExmplDsgn,
           probe_in6(0) => rxFrameClkReady_from_gbtExmplDsgn,
           probe_in7(0) => gbtRxReady_from_gbtExmplDsgn,
           probe_in8(0) => gbtRxReadyLostFlag_from_gbtExmplDsgn,
           probe_in9(0) => rxDataErrorSeen_from_gbtExmplDsgn,
           probe_in10(0) => rxExtrDataWidebusErSeen_from_gbtExmplDsgn,
           probe_in11(0) => '0',
           probe_in12(0) => latOptGbtBankRx_from_gbtExmplDsgn,
           probe_out0(0) => generalReset_from_user,
           probe_out1(0) => clkMuxSel_from_user,
           probe_out2 => open, --testPatterSel_from_user,
           probe_out3 => loopBack_from_user,
           probe_out4(0) => resetDataErrorSeenFlag_from_user,
           probe_out5(0) => resetGbtRxReadyLostFlag_from_user,
           probe_out6(0) => txIsDataSel_from_user,
           probe_out7(0) => manualResetTx_from_user,
           probe_out8(0) => manualResetRx_from_user
         );

   txILa: xlx_k7v7_vivado_debug
       port map (
          CLK => sysclk,
          PROBE0 => txData_from_gbtExmplDsgn,
          PROBE1 => txExtraDataWidebus_from_gbtExmplDsgn,
          PROBE2 => (others => '0'), --8b10b support removed
          PROBE3(0) => start_sca); --txIsDataSel_from_user);  
 
   rxIla: xlx_k7v7_vivado_debug
       port map (
          CLK => sysclk,
          PROBE0 => rxData_from_gbtExmplDsgn,
          PROBE1 => rxExtraDataWidebus_from_gbtExmplDsgn,
          PROBE2 => (others => '0'), --8b10b support removed
          PROBE3(0) => start_sca); --rxIsData_from_gbtExmplDsgn);

   -- On-board LEDs:                   
   -----------------    
    
   GPIO_LED_0_LS                                     <= latOptGbtBankTx_from_gbtExmplDsgn and latOptGbtBankRx_from_gbtExmplDsgn;          
   GPIO_LED_1_LS                                     <= txFrameClkPllLocked_from_gbtExmplDsgn;
   GPIO_LED_2_LS                                     <= mgtReady_from_gbtExmplDsgn;
   GPIO_LED_3_LS                                     <= gbtRxReady_from_gbtExmplDsgn;
   GPIO_LED_4_LS                                     <= gbtRxReadyLostFlag_from_gbtExmplDsgn;
   GPIO_LED_5_LS                                     <= rxDataErrorSeen_from_gbtExmplDsgn;
   GPIO_LED_6_LS                                     <= rxExtrDataWidebusErSeen_from_gbtExmplDsgn;
   GPIO_LED_7_LS                                     <= '0';
      
   --=====================--     
   -- Latency measurement --     
   --=====================--
   
   -- Clock forwarding:
   --------------------
   
   -- Comment: * The forwarding of the clocks allows to check the phase alignment of the different
   --            clocks using an oscilloscope.
   --
   --          * Note!! If RX DATA comes from another board with a different reference clock, 
   --                   then the TX_FRAMECLK/TX_WORDCLK domains are asynchronous with respect to the
   --                   TX_FRAMECLK/TX_WORDCLK domains.
   
   FMC1_HPC_LA02_P                                   <= txFrameClk_from_gbtExmplDsgn;
   FMC1_HPC_LA03_P                                   <= txWordClk_from_gbtExmplDsgn;
   --------------------------------------------------   
   FMC1_HPC_LA04_P                                   <= rxFrameClk_from_gbtExmplDsgn;  
   FMC1_HPC_LA05_P                                   <= rxWordClk_from_gbtExmplDsgn;
    
   USER_SMA_GPIO_P                                   <= txWordClk_from_gbtExmplDsgn when clkMuxSel_from_user = '1' else 
                                                        txFrameClk_from_gbtExmplDsgn;

   -- Pattern match flags:
   -----------------------
   
   -- Comment: * The latency of the link can be measured using an oscilloscope by comparing 
   --            the TX FLAG with the RX FLAG.
   --
   --          * The COUNTER TEST PATTERN must be used for this test. 
   
   FMC1_HPC_LA00_CC_P                                <= txMatchFlag_from_gbtExmplDsgn;
   FMC1_HPC_LA01_CC_P                                <= rxMatchFlag_from_gbtExmplDsgn;
   
   --=====================================================================================--   
end structural;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--